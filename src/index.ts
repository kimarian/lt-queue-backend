import * as express from 'express'
import * as cors from "cors"
import { LT } from './entity/LT'
import { ConnectionOptions, createConnection, BaseEntity } from 'typeorm'
import { QueryExpressionMap } from 'typeorm/query-builder/QueryExpressionMap'

const connect = async () => await createConnection({
    type: "sqlite",
    database: "lt.db",
    synchronize: true,
    logging: false,
    entities: [
        LT
    ]
})

let app = async () => {
    const app = express()
    app.use(express.json());
    app.use(express.urlencoded({extended: true}));
    app.use(cors())

    const connection = await connect();
    BaseEntity.useConnection(connection)

    app.get('/lt', async (req, res) => {
        try {
            if ("id" in req.query) {
                const ltid = req.query.id;
                const lts = await LT.findOne({id : ltid});
                res.send(JSON.stringify(lts.toSendable()));
            } else {
                const lts = await LT.find();
                const sendableLTs = lts.map(x => x.toSendable());
                res.send(JSON.stringify(sendableLTs));
            }
        } catch (e) {
            res.status(400).send("bad request.");
        }
    }) 
    .post('/lt/post', async (req, res) => {
        try {
            const newLT = LT.ConvertToR(req.body);
            await LT.save(newLT);
            res.status(200).send();
        } catch(e) {
            res.statu(400).send();
        }
    }) 
    .get('/lt/start', async (req, res) => {
        try {
            const ltid = req.query.id;
            
            const lt = await LT.findOne({id : ltid});

            lt.presenting = true;
            lt.startAt = new Date().getTime();

            await LT.save(lt);
            res.send(`LTID:${ltid} is started.`)
        } catch (e) {
            res.status(400).send("bad request.");
        }
    })
    .get('/lt/finish', async (req, res) => {
        try {
            const ltid = req.query.id;
            
            const lt = await LT.findOne({id : ltid});

            lt.presenting = false;
            lt.endAt = new Date().getTime();

            await LT.save(lt);

            res.send(`LTID:${ltid} is finished.`)
        } catch (e) {
            res.status(400).send("bad request.");
        }
    });

    const port: number = 5000;
    app.listen(port, () => console.log(`Example app listening on port:${port} !`))
}

app()
