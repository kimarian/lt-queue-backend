import {Entity, PrimaryGeneratedColumn, Column, BaseEntity} from "typeorm";

@Entity()
export class LT extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public title: string;

    @Column()
    public content: string;

    @Column()
    public thumbnailUrl: string;

    @Column()
    public date: number;

    @Column()
    public duration: number;

    @Column()
    public presenting: boolean;

    @Column({nullable:true})
    public startAt?: number;

    @Column({nullable:true})
    public endAt?: number;

    public static ConvertToR(data : any) {
        let lt = new LT();
        if ("id" in data) {
            lt.id = data.id;
        }
        
        let content = data.content;
        lt.title = content.title;
        lt.content = content.content;
        lt.thumbnailUrl = content.thumbnailUrl;

        let timeData = data.timeData;
        lt.date = timeData.date;
        lt.duration = timeData.duration;
        lt.presenting = timeData.presenting;

        if (timeData.startAt != undefined) {
            lt.startAt = timeData.startAt;
        } 

        if (timeData.endAt != undefined) {
            lt.endAt = timeData.endAt;
        }

        console.log(lt)

        return lt;
    }

    public toSendable() : any {
        let timeData : any = {
            "date": this.date,
            "duration" : this.duration,
            "presenting" : this.presenting
        }

        if (this.startAt != undefined) {
            timeData.startAt = this.startAt;
        } 

        if (this.endAt != undefined) {
            timeData.endAt = this.endAt;
        }

        return {
            "id" : this.id.toString(),
            "content" : {
                title: this.title,
                content: this.content,
                thumbnailUrl: this.thumbnailUrl,
            },
            "timeData" : timeData
        }
    }
}
