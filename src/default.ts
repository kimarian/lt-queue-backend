import "reflect-metadata";
import { createConnection, getConnectionOptions, BaseEntity } from "typeorm";
import { LT } from "./entity/LT"
import { create } from "domain";

export const sampleDatas = [
    {
        "id" : "0",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : true,
        }
    },
    {
        "id" : "1",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
        }
    },
    {
        "id" : "2",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
        }
    },
    {
        "id" : "3",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
        }
    },
    {
        "id" : "4",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
        }
    },
]

const connect = async () => await createConnection({
    type: "sqlite",
    database: "lt.db",
    synchronize: true,
    logging: false,
    entities: [
        LT
    ]
})

export const InitDB = async () => {
    let lts = sampleDatas.map(x => LT.ConvertToR(x));

    // --- TypeORMの設定
    const connection = await connect();
    // ActiveRecordパターンでTypeORMを使用する場合
    BaseEntity.useConnection(connection)
    lts.forEach(async x => {
        await LT.save(x);
    });
}

InitDB();
